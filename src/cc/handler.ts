import { DataItem } from './data'

export default (item: DataItem) => {
  const failure = 0.3
  return Math.random() < failure
    ? Promise.reject(new Error(`${item}: API failed`))
    : Promise.resolve(`${item}: API succeeded`)
}
