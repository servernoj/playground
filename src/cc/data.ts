import { minBy, filter } from 'lodash'

interface Item {
  id: number
  name: string
  score: number
}
export class DataItem implements Item {
  id: number
  name: string
  score: number
  constructor (item: Item) {
    this.id = item.id
    this.name = item.name
    this.score = item.score
  }

  get ready () {
    return this.score <= 0
  }

  enQueue (score: number) {
    this.score += score
  }

  deQueue () {
    this.score--
  }

  toString () {
    return `${this.id}`
  }
}

export class Data {
  private data: Array<DataItem>
  constructor (seed: Array<Item>) {
    this.data = seed.map(item => new DataItem(item))
  }

  get count () {
    return this.data.length
  }

  deQueue () {
    this.data.forEach(
      item => item.deQueue()
    )
  }

  toString () {
    return JSON.stringify(
      this.data.map(
        ({ id, score }) => ({ id, score })
      )
    )
  }

  getNext () {
    return minBy(
      filter(
        this.data,
        item => item.ready
      ), 'score'
    )
  }
}

const seed = [
  {
    id: 1,
    name: 'Peter',
    score: 0
  },
  {
    id: 2,
    name: 'John',
    score: 0
  },
  {
    id: 3,
    name: 'James',
    score: 0
  }
]

export default new Data(seed)
