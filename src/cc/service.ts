import data from './data'
import handler from './handler'

const dataCount = data.count
const setPoint = 7 * 1000

export default (rate: number) => async () => {
  const item = data.getNext()
  console.log(data.toString())
  if (!item) {
    data.deQueue()
  }
  else {
    try {
      const response = await handler(item)
      item.enQueue(4)
      console.log(response)
    }
    catch (error) {
      data.deQueue()
      console.error(error.message)
    }
  }
  console.log('-----------------------------------------')
}
